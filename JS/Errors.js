	function validator(theForm)
		{
		if (theForm.Humanity.checked == false)
		{
			alert("You really should consider becoming human instead of a spam computer.");
			return (false);
		}
		if (theForm.Title.value == "Title (Required)" || theForm.Title.value == "")
		{
		alert("Please enter a value for the \"Title\" field.");
		theForm.Title.focus();
		return (false);
		}
		if (theForm.Author.value == "Author (Required)" || theForm.Author.value == "")
		{
		alert("Please enter a value for the \"Author\" field.");
		theForm.Author.focus();
		return (false);
		}
		if (theForm.Name.value == "Name" || theForm.Name.value == "")
		{
		alert("Please enter a value for the \"Name\" field.");
		theForm.Name.focus();
		return (false);
		}
		return (true);
	}
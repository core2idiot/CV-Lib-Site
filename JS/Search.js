function Hide()
{
document.getElementByClass("Hide").style.visibility="hidden";
}
function open_win(url) {
	new_win = window.open(url,"new_win",'toolbar=1,location=1,directories=0,status=1,menubar=1,scrollbars=1,resizable=1,width=750,height=250');
	  if (new_win) 
	  {
	  new_win.focus();
	  }
}

function open_bare_win(url) {
	new_b_win = window.open(url,"new_b_win",'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,width=620,height=450');
	  if (new_win) 
	  {
		new_b_win.focus();
	  }
}

function open_hyperion_image_win(url) {
	hyperion_image_win = window.open(url,"hyperion_image_win",'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,width=600,height=450,screenX=410,screenY=210');
}

function open_hyperion_info_win(url) {
	hyperion_info_win = window.open(url,"hyperion_info_win",'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,width=400,height=450,screenX=0,screenY=210');
}

function open_win_timeout(url,timeout) {
	new_win = window.open(url,"new_win",'toolbar=0,location=0,directories=0,status=1,menubar=1,scrollbars=1,resizable=1,width=750,height=250');
	setTimeout("new_win.close()",timeout);
}

function open_help_win(url) {
       new_win = window.open(url,"help_win",'toolbar=1,location=1,directories=0,status=1,menubar=1,scrollbars=1,resizable=1,width=750,height=250');
	  if (new_win) 
	  {
	  new_win.focus();
	  }
}

function open_bounce_win(url) {
  new_win = window.open(url,"bounce_win",'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,width=620,height=450');
}

/* Function for icon-type search form action */
function orig_form_action()
{
	if (!document.searchform.search_type[0].checked)
	  {
	  return true;
	  }
	else
	  {
          document.url_form.url.value = "http://www.google.com/search?q=" + document.searchform.searchdata1.value + "&safe=vss&vss=1&sa=Google+Search";
          open_win(document.url_form.url.value);
	  return false;
	  }
}

/* Function for icon-type search form action */
function search_form_action()
{
	var search_engine_checked = 0;
	var temp_str;

	for (var i = 0; i < document.searchform.search_type.length; i++)
	  {
	  if (document.searchform.search_type[i].value == "searchengine")
	    if (document.searchform.search_type[i].checked)
	      search_engine_checked = 1;
	  }

	if (!search_engine_checked)
	  {
	  return true;
	  }
	else
	  {
          temp_str = document.searchform.searchdata1.value;
	  temp_str = temp_str.replace(/\s+/g,"+");
          document.url_form.url.value = "http://www.google.com/search?q=" + temp_str + "&safe=vss&vss=1&sa=Google+Search";
          open_win(document.url_form.url.value);
	  return false;
	  }
}


function getNonJavaHelpFileUrl(file)
{
	return "/iBistro_helps//" + file;
}

function put_help_button(file)
{
  document.writeln('<A HREF="javascript:open_help_win(getNonJavaHelpFileUrl(\'' + file + '\'))"><IMG SRC=/HELP.gif BORDER=0 ALT="Help"></A>');
}


function put_keepremove_button(ckey,value)
{
  document.writeln('<img name="ickey-' + ckey + '"');
  document.writeln('src="/clear.gif"');
  document.writeln('alt="' + value + '" border="0">');

  document.writeln('<input type="button"');
  if (value == 'Keep')
    {
    document.writeln('class="input_button1"');
    }
  else
    {
    document.writeln('class="input_button2"');
    }
  document.writeln('name="ckey-' + ckey + '"');
  document.writeln('id="ckey-' + ckey + '"');
  document.writeln('value="' + value + '" alt="' + value + '"');
  document.writeln('onClick="javascript:updatekeptlist(this,' + ckey + ');">');
}

function updatekeptlist(myButton,ckey)
{
  key = "i" + myButton.name;
  if (myButton.value == 'Keep')
    {
    myImage = new Image();
    myImage.src = '/uhtbin/cgisirsi.exe/?ps=AYYTgPrPck/ADAMS/0/125/ADD?kept-' + ckey + '=on';
    document.images[key].src=myImage.src;
    myImage = new Image();
    myImage.src = '/clear.gif';
    document.images[key].src=myImage.src;
    document.images[key].alt='Remove';
    myButton.value='Remove';
    myButton.style.color='#99CCFF';
    myButton.style.background='#000000';
    myButton.style.border.left='2px solid #4AA65B';
    myButton.style.border.top='2px solid #4AA65B'; 
    myButton.style.border.right='2px solid #1B5927'; 
    myButton.style.border.bottom='2px solid #1B5927';
    }
  else
    {
    myImage = new Image();
    myImage.src = '/uhtbin/cgisirsi.exe/?ps=YH7WUmAdJu/ADAMS/0/125/REMOVE?kept-' + ckey + '=on';
    document.images[key].src=myImage.src;
    myImage = new Image();
    myImage.src = '/clear.gif';
    document.images[key].src=myImage.src;
    document.images[key].alt='Keep';
    myButton.value='Keep';

    myButton.style.color='#000000';
    myButton.style.background='#99CCFF';
    myButton.style.border.left='2px solid #aaaa77';
    myButton.style.border.top='2px solid #aaaa77'; 
    myButton.style.border.right='2px solid #003366'; 
    myButton.style.border.bottom='2px solid #003366';
  }
}


function put_keepremove_all_button()
{
  document.writeln('<img name="ckey-ALL"');
  document.writeln('src="/clear.gif"');
  document.writeln('alt="' + 'Keep All on this Page' + '" border="0">');

  document.writeln('<input type="button"');
  document.writeln('class="input_button1"');
  document.writeln('style="width: 145;"');
  document.writeln('name="ckey-ALL"');
  document.writeln('value="' + 'Keep All on this Page' + '" alt="' + 'Keep All on this Page' + '"');
  document.writeln('onClick="javascript:updatekeptlist_all(this,keep_ckeys_array);">');
}

function updatekeptlist_all(myButton,ckeys_array)
{
  var ckey_str = new String("");

  for (i=0; i<ckeys_array.length; i++)
  {
    ckey_str = ckey_str + "kept-" + ckeys_array[i] + "=on&";
  }
  key = myButton.name;
  myImage = new Image();
  myImage.src = '/uhtbin/cgisirsi.exe/?ps=Fuq51wM1dv/ADAMS/0/125/ADD?' + ckey_str;
  document.images[key].src=myImage.src;

  // Change all individual Keep buttons to "marked"
  for (i=0; i<ckeys_array.length; i++)
  {
    ckey = ckeys_array[i];
    element_id = "ckey-" + ckey;
    document.images["i" + element_id].alt='Remove';
    document.getElementById(element_id).value='Remove';
    document.getElementById(element_id).style.color='#99CCFF';
    document.getElementById(element_id).style.background='#000000';
    document.getElementById(element_id).style.border.left='2px solid #4AA65B';
    document.getElementById(element_id).style.border.top='2px solid #4AA65B'; 
    document.getElementById(element_id).style.border.right='2px solid #1B5927'; 
    document.getElementById(element_id).style.border.bottom='2px solid #1B5927';
  }
  myImage = new Image();
  myImage.src = '/clear.gif';
  document.images[key].src=myImage.src;
}


function do_history(form)
  {
  // Based on the value of the srch_history <select> list, set the search input fields.
  var opt_idx;
  var i = 0;
  opt_idx = form.srch_history.selectedIndex;
  // The history "value" is formatted:  "term^label^library"
  valueArray = form.srch_history.options[opt_idx].value.split("^");
  // Set the search term
  form.searchdata1.value = valueArray[0];
  // Set the search type
  for (i<0; i<form.srchfield1.length; i++)
    {
    if (form.srchfield1.options[i].text == valueArray[1])
      {
      form.srchfield1.options[i].selected = true;
      break;
      }
    }
  // Set the library
  form.library.value = valueArray[2];
  }
function combine_fields()
{
    document.getElementById("searchdata3").value="{LEXILE}>" + document.getElementById("lexilemin").value + "<" + document.getElementById("lexilemax").value;
    //alert (document.getElementById("searchdata3").value);
}

/* Function for stripping the first character from input value */
function stripInput(objEvent,iChar)
{
  var ch;
  var objInput;
  var value;

  if (navigator.appName == 'Microsoft Internet Explorer')
    {
    objInput = objEvent.srcElement; 
    }
  else
    {
    objInput = objEvent.target;
    }
  value = objInput.value;

  ch = String.fromCharCode(iChar);

  if ( value.indexOf(ch) == 0)
    {
    objInput.value = objInput.validValue || value.substr(1);
    objInput.focus();
    objInput.select(); 
    }
  else 
    {
    objInput.validValue = objInput.value;
    }
}

function sendOpenURL(inISBN,inTITLE,inISSN,inGENRE)
{
	aWindow = window.open('','','toolbar=1,location=1,directories=0,status=1,menubar=1,scrollbars=1,resizable=1,width=750,height=250');
	
	if (aWindow.document.body == null)
	  {
	  aBody = aWindow.document.createElement('body');
	  aWindow.document.appendChild(aBody);
	  }

        nFORM=aWindow.document.createElement('form');
	nFORM.action='';
	nFORM.target="_self";
	nFORM.method="post";
	nFORM.name="OpenURL";

	if (inISBN.length != 0)
	  {
	  nISBN=aWindow.document.createElement('input');
	  nISBN.name='isbn';
	  nISBN.value=inISBN;
	  nISBN.type='hidden';

	  nFORM.appendChild(nISBN);
	  }

	if (inTITLE.length != 0)
	  {
	  nTITLE=aWindow.document.createElement('input');
	  nTITLE.name='title';
	  nTITLE.value=inTITLE;
	  nTITLE.type='hidden';

	  nFORM.appendChild(nTITLE);
	  } 

	if (inISSN.length != 0)
	  {
	  nISSN=aWindow.document.createElement('input');
	  nISSN.name='issn';
	  nISSN.value=inISSN;
	  nISSN.type='hidden';

	  nFORM.appendChild(nISSN);
	  }

	if (inGENRE.length != 0)
	  {
	  nGENRE=aWindow.document.createElement('input');
	  nGENRE.name='genre';
	  nGENRE.value=inGENRE;
	  nGENRE.type='hidden';

	  nFORM.appendChild(nGENRE);
	  }

	aWindow.document.body.appendChild(nFORM);

	nFORM.submit();

	aWindow.document.body.removeChild(nFORM);

}
